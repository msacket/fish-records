<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image_url', 'credit', 'title', 
        'weight', 'line_class', 'angler', 'location',
        'date', 'fight_time', 'lure_bait', 'tackle',
        'notes'
    ];
}
