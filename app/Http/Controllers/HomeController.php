<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    

    public function home(){
		$records = \App\Record::orderBy('rank', 'desc')->get();
	    return view('welcome', array(
	    	'records'=> $records
		));
	}
}
