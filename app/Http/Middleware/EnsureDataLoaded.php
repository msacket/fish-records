<?php

namespace App\Http\Middleware;

use Closure;
use App\Record;
use Symfony\Component\DomCrawler\Crawler;

class EnsureDataLoaded
{
    /**
     * Load the fishing records if there aren't any.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( Record::count() < 1 ){
            // there are no records, we need to load them.
            if(!$this->loadData()){
                dd('could not load data');
            }
        }
        return $next($request);
    }

    /**
     *  Loads the html from the sport fishing url and scrapes the
     *  content for the desired information; finally loading it into
     *  the record table.
     */
    public function loadData(){
        $url = 'http://www.sportfishingmag.com/top-100-world-record-fish';
        $html = $this->fetchDataFromUrl($url);
        $crawler = new Crawler($html);
        $records = $crawler->filter('ul.slides .file')->each(function ($node) {
            $result = array();

            $x = $node->filterXPath('.//div[@class="field-image"]//noscript//img');
            $result['image_url'] = count($x) ? trim($x->attr('src')) : null;

            $x = $node->filterXPath('.//div[@class="field-credit"]');
            $result['credit'] = count($x) ? trim($x->text()) : null;

            $x = $node->filterXPath('.//div[@class="field-title"]');
            $result['title'] = count($x) ? trim($x->text()) : null;

            $x = $node->filterXPath('.//div[@class="field-body"]/p[strong = "Weight"]');
            $result['weight'] = count($x) ? trim(str_replace('Weight:', '', $x->text())) : null;

            $x = $node->filterXPath('.//div[@class="field-body"]/p[strong = "Line Class"]');
            $result['line_class'] = count($x) ? trim(str_replace('Line Class:', '', $x->text())) : null;

            $x = $node->filterXPath('.//div[@class="field-body"]/p[strong = "Angler"]');
            $result['angler'] = count($x) ? trim(str_replace('Angler:', '', $x->text())) : null;

            $x = $node->filterXPath('.//div[@class="field-body"]/p[strong = "Location"]');
            $result['location'] = count($x) ? trim(str_replace('Location:', '', $x->text())) : null;

            $x = $node->filterXPath('.//div[@class="field-body"]/p[strong = "Date"]');
            $result['date'] = count($x) ? trim(str_replace('Date:', '', $x->text())) : null;

            $x = $node->filterXPath('.//div[@class="field-body"]/p[strong = "Fight Time"]');
            $result['fight_time'] = count($x) ? trim(str_replace('Fight Time:', '', $x->text())) : null;

            $x = $node->filterXPath('.//div[@class="field-body"]/p[strong = "Lure/Bait"]');
            $result['lure_bait'] = count($x) ? trim(str_replace('Lure/Bait:', '', $x->text())) : null;

            $x = $node->filterXPath('.//div[@class="field-body"]/p[strong = "Tackle"]');
            $result['tackle'] = count($x) ? trim(str_replace('Tackle:', '', $x->text())) : null;

            $x = $node->filterXPath('.//div[@class="field-body"]/p[strong = "Noteworthy"]');
            $result['notes'] = count($x) ? trim(str_replace('Noteworthy:', '', $x->text())) : null;

            return $result;
        });

        // remove the title slide.
        array_shift($records);

        //save the records
        for ($i=count($records); $i > 0; $i--) {
            $record = new Record($records[$i-1]);
            $record->rank = 100 - $i;
            $record->save();
        }
        return true;
    }

    /**
     *  Fetches and returns the data from the provided url.
     *  This is needed to get around the 403 denied when
     *  using file_get_contents
     */
    function fetchDataFromUrl($url){
        $ch = NULL;
        try {
            // create curl resource
            $ch = curl_init();

            // set url
            curl_setopt($ch, CURLOPT_URL, $url);

            //return the transfer as a string
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36');

            // $output contains the output string
            return curl_exec($ch);
        } finally {
            // close curl resource to free up system resources
            curl_close($ch);
        }
    }
}
