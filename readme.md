## Fishing Records

An application to extract the fishing records from http://www.sportfishingmag.com/top-100-world-record-fish, store them in a PostgreSQL database, and display them in an Laravel-based MVC application.

## Getting Started

1. `cd` to the project directory.
2. Launch the Homestead environment with `vagrant up`
3. SSH into instance `vagrant ssh`
4. 'cd /home/vagrant/fish-records`
5. `php artisan migrate`
6. Open application in a browser.