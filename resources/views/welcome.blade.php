<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Fishing Records</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="/css/main.css" rel="stylesheet" type="text/css">
        
    </head>
    <body>
        <div class="content">
            <h1>Fishing Records</h1>
            <hr>
            <div class="records-pane">
                @foreach ($records as $record)
                    <div class="record">
                        <span class="title">{{ $record->title }}</span>
                        <img src="{{ $record->image_url }}" style="max-width:100%;">
                        <div>
                            <span class="credit">{{ $record->credit }}</span>
                            <span class="attr"><strong>Weight:</strong> {{ $record->weight }}</span>
                            <span class="attr"><strong>Line Class:</strong> {{ $record->line_class }}</span>
                            <span class="attr"><strong>Angler:</strong> {{ $record->angler }}</span>
                            <span class="attr"><strong>Location:</strong> {{ $record->location }}</span>
                            <span class="attr"><strong>Date:</strong> {{ $record->date }}</span>
                            <span class="attr"><strong>Fight Time:</strong> {{ $record->fight_time }}</span>
                            <span class="attr"><strong>Lure/Bait:</strong> {{ $record->lure_bait }}</span>
                            <span class="attr"><strong>Tackle:</strong> {{ $record->tackle }}</span>
                            <span class="attr"><strong>Noteworthy:</strong> {{ $record->notes }}</span>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </body>
</html>
