<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rank');
            $table->text('image_url');
            $table->text('credit')->nullable();
            $table->text('title')->nullable();
            $table->text('weight')->nullable();
            $table->text('line_class')->nullable();
            $table->text('angler')->nullable();
            $table->text('location')->nullable();
            $table->text('date')->nullable();
            $table->text('fight_time')->nullable();
            $table->text('lure_bait')->nullable();
            $table->text('tackle')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('records');
    }
}
